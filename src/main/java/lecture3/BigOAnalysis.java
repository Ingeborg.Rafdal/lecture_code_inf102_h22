package lecture3;

import java.util.ArrayList;

public class BigOAnalysis {

	public static void main(String[] args) {
		order1(12,5);
		loopDeLoop(10);
		whatIf(17);
		recursivePrint(10);
	}
	
	public static void useList(ArrayList<Integer> numbers) {
		int a = numbers.get(0);
		int b = numbers.get(1);
		int sum = a+b;
		numbers.add(0,sum);
	}
	
	public static void order1(int a, int b) {
		int sum = a + b;
		System.out.println("Summen av "+a+" + "+b+" er "+sum);
	}
	
	public static void loopDeLoop(int n) {
		for(int i=0; i<n; i++) { 
			System.out.println("Loop de loop, flip flop."); 
			System.out.println("Flying in an aeroplane.");
		}
	}

	public static void whatIf(int n) {
		if(n%2==0) {
			loopDeLoop(n);
		}
		else {
			order1(n, n);
		}		
	}

	public static void whatIf2(int n) { 
		if(n<=10) {
			loopDeLoop(n);
		}
		else {
			order1(n, n);
		}		
	}

	public static void recursivePrint(int n) {
		if(n==1) {
			System.out.print(1);
		}
		else {
			System.out.print(n+" ");
			recursivePrint(n-1);
		}
	}

	public static String StringOperation() {
		String tekst = "Hello";
		char[] tekstArray = tekst.toCharArray();
		
		StringBuffer sb = new StringBuffer("Hello");
		ArrayList<String> sbArray = new ArrayList<String>();
		
		return tekst+"!";
	}

}
