package lecture1;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class HashDuplicateFinder implements DuplicateFinder<Object> {

	@Override
	public <T> T findDuplicate(List<T> list) {
		//create a set to keep all items seen so far
		Set<T> set = new HashSet<T>(list.size());  //O(n)

		for(T item : list) { //n iterasjoner
			if(set.contains(item))  //if seen before O(1)
				return item;		//O(1)
			else
				set.add(item);		//O(1)
		}
		return null;
	}
}